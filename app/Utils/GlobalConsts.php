<?php
namespace App\Utils;
class GlobalConsts {
    const ALL_BOOKS="books.";
    const DEFUALT_DRIVER="public_upload";
    const BOOK_DELETE=self::ALL_BOOKS . "delete";
    const BOOK_FORCE_DELETE=self::ALL_BOOKS . "deletePermanently";
    const BOOK_RESTORE=self::ALL_BOOKS . "restore";
    const DELETED_BOOKS=self::ALL_BOOKS . "deletedBooks";
    const CATEGORY_BOOKS=self::ALL_BOOKS . "categoryBooks";
    const BOOK_STORE=self::ALL_BOOKS . "store";
    const BOOK_EDIT=self::ALL_BOOKS . "edit";
    const BOOK_UPDATE=self::ALL_BOOKS . "uppdate";
    const BOOK_CONFIRM_DELETE = self::ALL_BOOKS . "confirmDelete";
    const PAGE_SIZE_VALUE=10;
    const PAGESIZE="pageSize";
}

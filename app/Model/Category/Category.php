<?php

namespace App\model\category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Book\Book;
class Category extends Model
{
    use SoftDeletes;
    protected $fillable=['name'];
    public function books(){
        return $this->hasMany(Book::class);
    }
}

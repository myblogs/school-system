<?php

namespace App\Model\Book;

use Illuminate\Database\Eloquent\Model;
use App\model\category\Category;
use Illuminate\Database\Eloquent\SoftDeletes;
class Book extends Model {
    use SoftDeletes;
    protected $fillable = ['name', 'category_id', 'hash_book', 'hash_image', 'description'];

    public function category() {
        return $this->BelongsTo(Category::class);
    }

}

<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookValidation;
use App\Repository\Book\BookRepository;
use App\Repository\Category\CategoryRepository;
use App\Http\Controllers\Utils\ControllerUtil;
use App\Http\Requests\UpdateValidation;
use Illuminate\Support\Facades\Storage;
use App\Utils\GlobalConsts;

class BookController extends Controller {

    private $bookRepository;
    private $categoryRepository;
    private $categories = 'categories';
    private $imagesPath = "upload/images";
    private $books = "books";
    private $hashImage = "hash_image";
    private $pageSize = 12;

    function __construct(BookRepository $bookRepository, CategoryRepository $categoryRepository) {
        $this->bookRepository = $bookRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function store(BookValidation $request) {
        $book = $this->bookRepository->createBook($this->uploadAndAddToInputs($request));
        return $this->getAllBooks()->with('book', $book);
    }

    public function edit($bookId) {
        $book = $this->bookRepository->getBook($bookId);
        ControllerUtil::abort404IfNotFound($book);
        return view("$this->books.update")
                        ->with('book', $book)
                        ->with($this->categories, $this->categoryRepository->getAllCategories());
    }

    public function update($bookId, UpdateValidation $request) {
        $this->bookRepository->updateBook($this->getUpdatedBook($request), $bookId);
        return $this->getAllBooks();
    }

    public function delete($bookId) {
        ControllerUtil::abort404IfNotFound($this->bookRepository->deleteBook($bookId));
    }

    public function restore($bookId) {
        ControllerUtil::abort404IfNotFound($this->bookRepository->restoreBook($bookId));
    }

    function verifyDelete($bookId) {
        return view('books.confirm_delete', ['bookId' => $bookId]);
    }

    public function deletePermanently($bookId) {
        $book = $this->bookRepository->deletePermanently($bookId);
        ControllerUtil::abort404IfNotFound($book);
        $this->deleteFiles([$book->hash_image, $book->hash_book]);
        return $this->getAllBooks()->with('book', $book);
    }

    public function getAllBooks() {
        $books = $this->bookRepository->getAllBooks($this->pageSize);
        return view("$this->books.managing_books")
                        ->with($this->books, $books)
                        ->with($this->categories, $this->categoryRepository->getAllCategories());
    }

    public function getDeletedBooks() {
        $books = $this->bookRepository->getDeletedBooks($this->pageSize);
        return view("$this->books.managing_books")
                        ->with($this->books, $books)
                        ->with($this->categories, $this->categoryRepository->getAllCategories());
    }

    public function getCategoryBooks($categoryId) {
        $books = $this->categoryRepository->getCategoryBooks($categoryId, $this->pageSize);
        ControllerUtil::abort404IfNotFound($books);
        return view("$this->books.managing_books")
                        ->with($this->books, $books)
                        ->with($this->categories, $this->categoryRepository->getAllCategories());
    }

    //Utils
    private function uploadAndAddToInputs($request) {
        $inputs = $request->input();
        $inputs['hash_book'] = $request->book->store('upload/files');
        $inputs[$this->hashImage] = $request->image->store($this->imagesPath);
        return $inputs;
    }

    public function getUpdatedBook($request) {
        $updatedInputs = $request->except('_token', '_method', 'image');
        if ($request->image === null) {
            return $updatedInputs;
        }
        Storage::disk(GlobalConsts::DEFUALT_DRIVER)->delete($request->input($this->hashImage));
        $updatedInputs[$this->hashImage] = $request->image->store($this->imagesPath);
        return $updatedInputs;
    }

    private function deleteFiles($files) {
        foreach ($files as $file) {
            Storage::disk(GlobalConsts::DEFUALT_DRIVER)->delete($file);
        }
    }

}

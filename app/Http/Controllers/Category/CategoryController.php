<?php

namespace App\Http\Controllers\category;
use App\Repository\Category\CategoryRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\NameValidation;
use App\Http\Controllers\Utils\ControllerUtil;
class CategoryController extends Controller {
    private $categoryRepositroy;
    private $managing_categories = 'category.managing_categories';

    public function __construct(CategoryRepository $CategoryRepositroy) {
        $this->categoryRepositroy = $CategoryRepositroy;
    }

    public function index() {
        $data['categories'] = $this->categoryRepositroy->getAllCategories();
        return view($this->managing_categories, $data);
    }

    public function getDeletedCategories() {
        $data['categories'] = $this->categoryRepositroy->getTrashedCategories();
        return view($this->managing_categories, $data);
    }

    public function store(NameValidation $request) {
        $this->categoryRepositroy->create($request->input());
        return $this->index();
    }

    public function edit($categoryId) {
        $category = $this->categoryRepositroy->getCategory($categoryId);
        ControllerUtil::abort404IfNotFound($category);
        return view('category.update', ['category' => $category]);
    }

    public function update($categoryId, NameValidation $request) {
        $this->categoryRepositroy->updateCategory($request->only('name'), $categoryId);
        return $this->index();
    }

    public function delete($categoryId) {
        ControllerUtil::abort404IfNotFound($this->categoryRepositroy->deleteCategory($categoryId));
    }

    public function restore($categoryId) {
        ControllerUtil::abort404IfNotFound( $this->categoryRepositroy->restoreCategory($categoryId));
    }

    public function deletePermanently($categoryId) {
        ControllerUtil::abort404IfNotFound($this->categoryRepositroy->deletePermanently($categoryId));
        return $this->getDeletedCategories();
    }

    public function verifyDelete($categoryId) {
        return view('category.confirm_delete', ['categoryId' => $categoryId]);
    }


}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryRepository
 *
 * @author Ahmed Atef
 */

namespace App\Repository\Category;

use App\model\category\Category;

class CategoryRepository {

    private $category;

    public function create($inputs) {
        Category::create($inputs);
    }

    public function getCategory($categoryId) {
        return Category::find($categoryId);
    }

    public function updateCategory($categoryData, $categoryId) {
        Category::where('id', $categoryId)->update($categoryData);
    }

    public function deleteCategory($categoryId) {
        $this->category = Category::find($categoryId);
        if ($this->category !== null) {
            return $this->category->delete();
        }
    }

    public function getAllCategories() {
        return Category::get();
    }

    public function getTrashedCategories() {
        return Category::onlyTrashed()->get();
    }

    public function restoreCategory($categoryId) {
        $this->category = Category::withTrashed()->find($categoryId);
        if ($this->category !== null) {
            return $this->category->restore();
        }
    }

    public function deletePermanently($categoryId) {
        $this->category = Category::withTrashed()->find($categoryId);
        if ($this->category !== null) {
            return $this->category->forceDelete();
        }
    }
    public function getCategoryBooks($categoryId,$pageSize){
        $this->category=Category::with('books')->find($categoryId);
        if($this->category!==null){
            return $this->category->books()->withTrashed()->paginate($pageSize);
        }
    }
    
}

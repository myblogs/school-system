<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BookRepository
 *
 * @author Ahmed Atef
 */

namespace App\Repository\Book;

use App\Model\Book\Book;

class BookRepository {

    private $book;
    private $category='category';
    public function createBook($inputs) {
        return Book::create($inputs);
    }

    public function getBook($categoryId) {
        return Book::has($this->category)->find($categoryId);
    }

    public function updateBook($bookData, $bookId) {
        Book::where('id', $bookId)->update($bookData);
    }

    public function getAllBooks($pageSize) {
        return Book::has($this->category)->with($this->category)->paginate($pageSize);
    }

    public function deleteBook($bookId) {
        $this->book = Book::find($bookId);
        if ($this->book !== null) {
            return $this->book->delete();
        }
    }

    public function restoreBook($bookId) {
        $this->book = Book::withTrashed()->find($bookId);
        if ($this->book !== null) {
            return $this->book->restore();
        }
    }

    public function getDeletedBooks($pageSize) {
        return Book::has($this->category)->with($this->category)->onlyTrashed()->paginate($pageSize);
    }

    public function deletePermanently($bookId) {
        $this->book = Book::withTrashed()->find($bookId);
        if ($this->book !== null) {
            $this->book->forceDelete();
            return $this->book;
        }
    }
}

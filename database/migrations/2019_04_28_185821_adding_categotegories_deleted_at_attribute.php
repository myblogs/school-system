<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingCategotegoriesDeletedAtAttribute extends Migration {
    public function up() {
        Schema::table('categories', function (Blueprint $table) {
            $table->softDeletes();
        });
    }
    public function down() {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }

}

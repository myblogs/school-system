@extends('layouts.app')
@section('title','Managing books')
@section('content')
<div class="managing-books">
    <a href="#managing-books" id="up" class="up btn btn-link fixed-bottom"><span class="btn btn-success"><i class="fas fa-arrow-up"></i> Up</span></a>
    <div class="options">
        <a href="{{route($globalConsts::ALL_BOOKS)}}"><i class="fas fa-database"></i> Books</a>
        <a href="{{route($globalConsts::DELETED_BOOKS)}}"><i class="fa fa-trash" aria-hidden="true"></i>
            Deleted books</a>
        <a href="javascript::void(0)" onclick="openNav()"><i class="fas fa-list"></i> categories</a>
    </div>
    <div  id="mySidenav" class="sidenav">
        <div class="header">
            <div class="row">
                <div class="col-8"><i class="fas fa-tag"></i> Categories</div>
                <div class="col-3"><a href="javascript:void(0)"  onclick="closeNav()"><i class="fas fa-times"></i></a></div>
            </div>
        </div>
        @foreach($categories as $category)
        <a  href="{{route($globalConsts::CATEGORY_BOOKS,$category->id)}}">{{$category->name}}</a>
        <hr>
        @endforeach
        @for($i=0;$i<7-count($categories);$i++)
        <a style="color:#f9f9f9">category</a>
        <hr>
        @endfor
    </div>
    <div class="create-book">
        <div class="header">
            <div class="row">
                <div class="col-12"><i class="fas fa-tag"></i> Create book</div>
            </div>
        </div>

        <form action="{{route($globalConsts::BOOK_STORE)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Book name">
                    <div class="text-danger">{{$errors->first('name')}}</div>
                </div>
            </div>
            <div class="row files">
                <div class="image col-md-4">
                    <div class="custom-file">
                        <input name="image" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose image</label>
                        <div class="text-danger">{{$errors->first('image')}}</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="custom-file">
                        <input type="file" name="book" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose book</label>
                        <div class="text-danger">{{$errors->first('book')}}</div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-4">
                    <select name="category_id" class="form-control">
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    <div class="text-danger">{{$errors->first('category_id')}}</div>
                </div>
            </div>
            <div class="row book-description">
                <div class="col-md-8">
                    <textarea placeholder="Book description" name="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="submit btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <div class="header">
        <div class="row">
            <div class="col-12">
                <i class="fas fa-tag"></i> Books data
            </div>
        </div>
    </div>

    <div class="table-data">
        <div class="row head">
            <div class="col-4">
                name
            </div>
            <div class="col-4">
                category
            </div>

            <div class="col-4 text-center">
                actions
            </div>

        </div>
        <hr class="head">
        @foreach($books as $book)
        <div id ="{{$book->id . '1'}}">
            <div  class="row">
                <div id ="{{$book->id . '2'}}"  class="col-4">
                    @if($book->trashed())
                    {{$book->name}}
                    @else
                    <a  href="{{route($globalConsts::BOOK_EDIT,$book->id)}}"><i class="far fa-edit"></i> {{$book->name}}</a>
                    @endif
                </div>
                <div id ="{{$book->id . '3'}}" class="col-4">
                    {{$book->category->name}}
                </div>
                <div id ="{{$book->id . '6'}}"  class="col-4 text-center">
                    @if($book->trashed())
                    <a class="text-success" onclick="performAjax({{$book-> id}})" id="{{$book->id . '4'}}" href='javascript:void(0)' data-url="{{route($globalConsts::BOOK_RESTORE,$book->id)}}"><i class="btn btn-primary btn-sm  fas fa-undo"></i></a>
                    <a href="{{route($globalConsts::BOOK_CONFIRM_DELETE,$book->id)}}" class="text-danger"><i class="btn btn-danger btn-sm fas fa-times "></i></a>
                    @else
                    <a onclick="performAjax({{$book-> id}})" id="{{$book->id . '4'}}" class="text-danger" href='javascript:void(0)' data-url="{{route($globalConsts::BOOK_DELETE,$book->id)}}"><i class="btn btn-danger btn-sm fas fa-times "></i></a>
                    @endif
                </div>
            </div>
            <hr id ="{{$book->id . '5'}}" >
        </div>
        @endforeach
    </div>
</div>
@endsection
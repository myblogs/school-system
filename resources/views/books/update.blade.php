@extends('layouts.app')
@section('title','Update book')
@section('content')
<div class="update-book">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i> Update book</div>
        </div>
    </div>

    <form action="{{route($globalConsts::BOOK_UPDATE,$book->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <input name='hash_image' value="{{$book->hash_image}}" style="visibility:hidden"/>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="name">
                            <input type="text" class="form-control" id="name" name="name" value="{{$book->name}}">
                            <div class="text-danger">{{$errors->first('name')}}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="categories">
                            <select  name="category_id" class="form-control">
                                @foreach($categories as $category)
                                <option @if($category->id===$book->category_id){{'selected'}}@endif value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="description">
                            <div >
                                <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$book->description}}      </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="image">
                    <div class="header">
                        <div class="row">
                            <div class="col-12"><i class="fas fa-tag"></i> Book image</div>
                        </div>
                    </div>
                    <div>
                        <img src='{{asset("$book->hash_image")}}'>
                    </div>
                    <div class="custom-file">
                        <input name="image" type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose image</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="submit row">
            <div class="col-12">
                <button type="submit" class="submit btn btn-success">Submit</button>
                <a class="btn btn-link" href="{{URL::previous()}}">Back</a>
            </div>
        </div>
    </form>
</div>
@endsection

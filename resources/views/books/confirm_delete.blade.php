@extends('layouts.app')
@section('title','Delete confirmation')
@section('content')
<div class="category-delete-confirmation">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i>@yield('title')</div>
        </div>
    </div>
    <div class="confirm-delete-message text-center">
        <h3 class="text-danger">Are you sure you want permanently delete this</h3>
        <a class="btn btn-outline-danger" href="{{route($globalConsts::BOOK_FORCE_DELETE,$bookId)}}" >Yes</a>
        <a class="btn btn-outline-primary" href="{{URL::previous()}}">No</a>
    </div>
</div>
@endsection

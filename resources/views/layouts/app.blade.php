<!DOCTYPE html>
<html>
    <head>
        <title>School system - @yield('title')</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('css/book.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('css/category.css')}}" rel="stylesheet" type="text/css"/>
        <script src="{{asset('js/book.js')}}"></script>
        <script src="{{asset('js/app.js')}}"></script>
    </head>
    <body>
        <nav  class="basic-nav navbar navbar-expand-sm bg-cover navbar-light fixed-top">
            <a class="navbar-brand" href="#">
                <i class="fas fa-home"></i>Home
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="\category">
                            <i class="fa fa-list-alt" aria-hidden="true"></i> Managing categories
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{route($globalConsts::ALL_BOOKS)}}">
                            <i class="fa fa-book" aria-hidden="true"></i> Managing Books
                        </a>
                    </li>
                </ul>
            </div> 
        </nav>
        <div class="wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </body>
</html>
@extends('layouts.app')
@section('title','Category updating')
@section('content')
<div class="update-category">
    <div class="row">
        <div class="col-2">

        </div>
        <div class="col-8">
            <div class="header">
                <div class="row">
                    <div class="col-12"><i class="fas fa-tag"></i> @yield('title')</div>
                </div>
            </div>
            <form action='\category/{{$category->id}}' method="post">
                <div class="form-group">
                    @csrf
                    @method('put')
                    <input type="text" class="form-control" id="name" name="name" 
                           value="{{$category->name}}">
                    <div class="text-danger" style="padding:15px;">{{$errors->first('name')}}</div>
                </div>
                <div class="text-center ">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <a class="btn btn-link" href="{{URL::previous()}}">Back</a>
                </div>
            </form>

        </div>
        <div class="col-2">

        </div>
    </div>
</div>
@endsection
@extends('layouts.app')
@section('title','Managing categories')
@section('content')
<div class="category-managing" id="category-managing">
    <a href="#category-managing" id="up" class="up btn btn-link fixed-bottom"><span class="btn btn-success"><i class="fas fa-arrow-up"></i> Up</span></a>
    <div class="options">
        <a href="\category"><i class="fas fa-database"></i> Categories</a>
        <a href="\category/deletedCategories"><i class="fa fa-trash" aria-hidden="true"></i>
            Deleted categories</a>
    </div>
    <div class="create-category">
        <div class="header">
            <div class="row">
                <div class="col-12"><i class="fas fa-tag"></i> Create category</div>
            </div>
        </div>
        <form class="form-inline" action="\category" method="post">
            @csrf
            <input type="text" class="form-control mb-2 mr-sm-2" name='name' id="name">
            <button  type="submit" class="btn btn-success mb-2">Submit</button>
        </form>
        <div class="text-danger error-message">{{$errors->first('name')}}</div>
    </div>
    <div class="header">
        <div class="row">
            <div class="col-12">
                <i class="fas fa-tag"></i> @yield('title')
            </div>
        </div>
    </div>
    <div class="table-data">
        @foreach($categories as $category)
        <div id ="{{$category->id . '1'}}">
            <div  class="row">
                <div id ="{{$category->id . '2'}}"  class="col-8">
                    @if($category->trashed())
                    {{$category->name}}
                    @else
                    <a  href="\category/{{$category->id}}/edit"><i class="far fa-edit"></i> {{$category->name}}</a>
                    @endif
                </div>
                <div id ="{{$category->id . '3'}}" class="col-4 text-center">
                    @if($category->trashed())
                    <a onclick="performAjax({{$category-> id}})" id="{{$category->id . '4'}}" class="text-success"  href='javascript:void(0)' data-url="\category/{{$category->id}}/restore"><i class="btn btn-primary btn-sm  fas fa-undo"></i></a>
                    <a  href="\category/{{$category->id}}/verifyDelete" class="text-danger"><i class="btn btn-danger btn-sm fas fa-times "></i></a>
                    @else
                    <a id="{{$category->id . '4'}}" onclick="performAjax({{$category-> id}})" class="text-danger" href="javascript:void(0)" data-url="\category/{{$category->id}}/delete"><i class="btn btn-danger btn-sm fas fa-times "></i></a>
                    @endif
                </div>
            </div>
            <hr id ="{{$category->id . '5'}}" >
        </div>
        @endforeach
    </div>
</div>
@endsection

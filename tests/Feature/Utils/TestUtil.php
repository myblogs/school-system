<?php

namespace Tests\Feature\Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestUtils
 *
 * @author Ahmed Atef
 */
use Tests\TestCase;
use App\model\category\Category;

class TestUtil extends TestCase {

    public static function assertViewExistance($view, $statusCode, $response) {
        $response->assertStatus($statusCode);
        $response->assertViewIs($view);
    }

    public static function createCategory($categoryData) {
        return Category::create(static::getCategory($categoryData))->id;
    }

    public static function getCategory($categoryData) {
        return ['name' => $categoryData[0]];
    }
    public static function assertCategoryExistance($categoryData, $categoryId) {
        $category = Category::find($categoryId);
        static::assertEquals($category->name, $categoryData['name']);
    }

    public static function assertViewWithData($response,$view,$dataName,$data,$pagination=null,$size=null) {
        static::assertViewExistance($view,200, $response);
        $modelData = $response->viewData($dataName)->toArray();
        if($pagination==="pagination"){
            $modelData=$modelData['data'];
        }
        $expectedSize = 1;
        if($size!==null){
            $expectedSize=$size;
        }
        if ($data === null) {
            $expectedSize = 0;
        }
        else {
            static::assertEquals($modelData[0]['name'], $data);
        }
        static::assertEquals(count($modelData), $expectedSize);
    }

}

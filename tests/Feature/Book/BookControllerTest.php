<?php

namespace Tests\Feature\category;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\Utils\TestUtil;
use Illuminate\Http\UploadedFile;
use App\Model\Book\Book;
use Illuminate\Support\Facades\Storage;
use App\Utils\GlobalConsts;

class BookControllerTest extends TestCase {

    private $statusCodes = [200, 422, 404];
    private $books = 'books';
    private $bookData;
    private $nullBookData = [null, null, null];
    private $uploadedFile;
    private $uploadedImage;
    private $createView;
    private $updateView;
    private $invalidBookData;
    private $validBookData;
    private $imageErrorMessage = "The image must be an image.";
    private $nonNumericCategoryData;
    private $validCategoryData = ['category1'];
    private $bookId;
    private $nonExistBookId = 100;
    private $requiredErrorMessage = 'This field is required';
    private $book;
    private $categories = 'categories';
    private $managingBooksView;
    private $pagination = "pagination";

    use RefreshDatabase;

    public function setUp(): void {
        $this->uploadedFile = UploadedFile::fake()->create('file.pdf');
        $this->uploadedImage = UploadedFile::fake()->image('image.jpg');
        $this->createView = "$this->books.create";
        $this->updateView = "$this->books.update";
        $this->managingBooksView = "$this->books.managing_books";
        $this->validBookData = ['name', 1, $this->uploadedFile, $this->uploadedImage];
        $this->invalidBookData = [$this->validBookData[0], 0, $this->uploadedFile, $this->uploadedFile];
        $this->nonNumericCategoryData = $this->validBookData;
        $this->nonNumericCategoryData[1] = "Not number";
        $this->bookId = 1;
        parent::setUp();
    }

    public function tearDown(): void {
        $allFiles = Storage::disk(GlobalConsts::DEFUALT_DRIVER)->allFiles('upload');
        Storage::disk(GlobalConsts::DEFUALT_DRIVER)->delete($allFiles);
        parent::tearDown();
    }

    public function testGivenNullBookData_WhenCreate_thenWillGivesErrors() {
        //Given
        $this->bookData = $this->getBook($this->nullBookData);
        //When
        $this->response = $this->postJson(route(GlobalConsts::BOOK_STORE), $this->bookData);
        //Then
        $this->assertNullDataInvalidation($this->response);
    }

    public function testGivenInvalidBookData_WhenCreate_thenWillGivesErrors() {
        //Given
        $this->bookData = $this->getBook($this->invalidBookData);
        //When
        $this->response = $this->postJson(route(GlobalConsts::BOOK_STORE), $this->bookData);
        //Then
        $this->assertInvalidData($this->response);
    }

    public function testGivenNonNumericCategoryId_WhenCreate_thenWillGivesErrors() {
        //Given
        $this->bookData = $this->getBook($this->nonNumericCategoryData);
        //When
        $this->response = $this->postJson(route(GlobalConsts::BOOK_STORE), $this->bookData);
        //Then
        $this->assertNonNumericCategoryInvalidation($this->response);
    }

    public function testGivenValidBookData_WhenCreate_thenWillBeCreated() {
        //Given
        $categoryId = TestUtil::createCategory($this->validCategoryData);
        $this->bookData = $this->getBook($this->validBookData, $categoryId);
        //When
        $this->response = $this->postJson(route(GlobalConsts::BOOK_STORE), $this->bookData);
        //Then
        $this->assertBookExistance($this->bookData, $this->response->viewData('book')->id);
        TestUtil::assertViewWithData($this->response, $this->managingBooksView, $this->books
                , $this->validBookData[0], $this->pagination);
        TestUtil::assertViewWithData($this->response, $this->managingBooksView
                , $this->categories, $this->validCategoryData[0]);
    }

    public function testGivenExistBook_WhenReachingUpdateView_thenWillBeReached() {
        //Given
        $this->bookId = $this->createBook($this->validBookData)->id;
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_EDIT,$this->bookId));
        //Then
        TestUtil::assertViewExistance($this->updateView, $this->statusCodes[0], $this->response);
        TestUtil::assertViewWithData($this->response, $this->updateView, $this->categories, $this->validCategoryData[0]);
        $this->assertBookExistance($this->response->viewData('book'), $this->bookId);
    }

    public function testGivenNonExistBook_WhenReachingUpdateView_thenWillAbort() {
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_EDIT,$this->nonExistBookId));
        //Then
        $this->response->assertStatus($this->statusCodes[2]);
    }

    public function testGivenNullBookData_WhenUpdate_thenWillGivesErrors() {
        //Given
        $this->bookData = $this->getUpdateBook($this->nullBookData);
        //When
        $this->response = $this->putJson(route(GlobalConsts::BOOK_UPDATE,$this->bookId),$this->bookData);
        //Then
        $this->assertNullUpdateInvalidation($this->response);
    }

    public function testGivenInvalidBookData_WhenUpdate_thenWillGivesErrors() {
        //Given
        $this->bookData = $this->getUpdateBook($this->invalidBookData);
        //When
        $this->response = $this->putJson(route(GlobalConsts::BOOK_UPDATE,$this->bookId),$this->bookData);
        //Then
        $this->assertUpdateInvalid($this->response);
    }

    public function testGivenNonNumericCategoryId_WhenUpdate_thenWillGivesErrors() {
        //Given
        $this->bookData = $this->getUpdateBook($this->nonNumericCategoryData);
        //When
        $this->response = $this->putJson(route(GlobalConsts::BOOK_UPDATE,$this->bookId),$this->bookData);
        //Then
        $this->assertNonNumericCategoryInvalidation($this->response);
    }

    public function testGivenValidWithNoImageUpdate_WhenUpdate_thenWillUpdate() {
        //Given
        $this->book = $this->createBook($this->validBookData);
        $updatedBookData = $this->getUpdateBook(['Updated category', $this->book->category_id, $this->book->hash_image, null]);
        //When
        $this->response = $this->putJson(route(GlobalConsts::BOOK_UPDATE,$this->book->id),$updatedBookData);
        //Then
        $this->assertBookUpdated($updatedBookData, $this->book->id);
        TestUtil::assertViewWithData($this->response, $this->managingBooksView, $this->books
                , $updatedBookData['name'], $this->pagination);
        TestUtil::assertViewWithData($this->response, $this->managingBooksView
                , $this->categories, $this->validCategoryData[0]);
    }

    public function testGivenValidUpdate_WhenUpdate_thenWillUpdate() {
        //Given
        $updatedImage = UploadedFile::fake()->image('updatedImage.jpg');
        $this->book = $this->createBook($this->validBookData);
        $updatedBookData = $this->getUpdateBook(['Updated category', $this->book->category_id
            , $this->book->hash_image, $updatedImage]);
        //When
        $this->response = $this->putJson(route(GlobalConsts::BOOK_UPDATE,$this->book->id),$updatedBookData);
        //Then
        $this->assertBookExistance($updatedBookData, $this->book->id);
        Storage::disk(GlobalConsts::DEFUALT_DRIVER)->assertMissing($updatedBookData['hash_image']);
        TestUtil::assertViewWithData($this->response, $this->managingBooksView, $this->books
                , $updatedBookData['name'], $this->pagination);
    }

    public function testGivenBooks_WhenAdminRequestPagesToManage_thenWillReturned() {
        //Given
        $this->createBook($this->validBookData);
        //When
        $this->response = $this->get(route(GlobalConsts::ALL_BOOKS));
        //Then
        TestUtil::assertViewWithData($this->response, $this->managingBooksView, $this->books
                , $this->validBookData[0], $this->pagination);
    }

    public function testGivenDeletedBooks_WhenAdminRequestPagesToManage_thenWillReturned() {
        //Given
        $this->createAndDelete($this->validBookData);
        //When
        $this->response = $this->get(route(GlobalConsts::DELETED_BOOKS));
        //Then
        TestUtil::assertViewWithData($this->response, $this->managingBooksView, $this->books
                , $this->validBookData[0], $this->pagination);
    }

    public function testGivenNonExistBook_WhenDelete_thenWillAbort() {
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_DELETE,$this->nonExistBookId));
        //Then
        $this->response->assertStatus($this->statusCodes[2]);
    }

    public function testGivenExistBook_WhenDelete_thenWillDeleted() {
        //Given
        $this->book = $this->createBook($this->validBookData);
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_DELETE,$this->book->id));
        //Then
        $this->assertNotNull(Book::withTrashed()->find($this->book->id));
        $this->assertNull(Book::find($this->book->id));
    }

    public function testGivenNonExistDeletedBook_WhenRestore_thenWillAbort() {
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_RESTORE,$this->nonExistBookId));
        //Then
        $this->response->assertStatus($this->statusCodes[2]);
    }

    public function testGivenExistDeletedBook_WhenRestore_thenWillRestore() {
        //Given
        $this->book = $this->createAndDelete($this->validBookData);
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_RESTORE,$this->book->id));
        //Then
        $this->assertBookExistance($this->getBook($this->validBookData,$this->book->category_id), $this->book->id);
    }

    public function testGivenNonExistDeletedBook_WhenForceDelete_thenWillAbort() {
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_FORCE_DELETE,$this->nonExistBookId));
        //Then
        $this->response->assertStatus($this->statusCodes[2]);
    }

    public function testGivenExistDeletedBook_WhenForceDelete_thenWillDelete() {
        //Given
        $this->book = $this->createAndDelete($this->validBookData);
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_FORCE_DELETE,$this->book->id));
        //Then
        TestUtil::assertViewWithData($this->response, $this->managingBooksView, $this->books
                , null, $this->pagination);
        TestUtil::assertViewWithData($this->response, $this->managingBooksView
                , $this->categories, $this->validCategoryData[0]);
        Storage::disk(GlobalConsts::DEFUALT_DRIVER)->assertMissing($this->response->viewData('book')->hash_image);
        Storage::disk(GlobalConsts::DEFUALT_DRIVER)->assertMissing($this->response->viewData('book')->hash_book);
    }

    public function GivenNonExistCategory_WhenRequestCategoryBooks_thenWillAbort() {
        //When
        $this->response = $this->get(route(GlobalConsts::CATEGORY_BOOKS,$this->nonExistBookId));
        //Then
        $this->response->assertStatus($this->statusCodes[2]);
    }

    public function testGivenExistCategory_WhenRequestCategoryBooks_thenWillReturned() {
        $this->book = $this->createBook($this->validBookData);
        $this->createBook($this->validBookData);
        //When
        $this->response = $this->get(route(GlobalConsts::CATEGORY_BOOKS,$this->book->category_id));
        //Then
        TestUtil::assertViewWithData($this->response, $this->managingBooksView, $this->books
                , $this->validBookData[0], $this->pagination);
        TestUtil::assertViewWithData($this->response, $this->managingBooksView
                , $this->categories, $this->validCategoryData[0],null,2);
    }

    public function testWhenReachingConfirmDeleteView_thenWillBeReached() {
        //When
        $this->response = $this->get(route(GlobalConsts::BOOK_CONFIRM_DELETE,$this->bookId));
        //Then
        TestUtil::assertViewExistance("$this->books.confirm_delete", $this->statusCodes[0], $this->response);
        $this->assertEquals($this->response->viewData('bookId'),$this->bookId);
    }

    //Utils
    private function getBook($categoryData, $categoryId = null) {
        if ($categoryId !== null) {
            $categoryData[1] = $categoryId;
        }
        $book['name']=$categoryData[0];
        $book['category_id']=$categoryData[1];
        $book['book']=$categoryData[2];
        if(count($categoryData)!==3){
            $book['image']=$categoryData[3];
        }
        $book['description']='Description';
        return $book;
    }

    private function getUpdateBook($categoryData) {
        $updatedBook['name']=$categoryData[0];
        $updatedBook['category_id']=$categoryData[1];
        $updatedBook['hash_image']=$categoryData[2];
        if(count($categoryData)!==3){
            $updatedBook['image']=$categoryData[3];
        }
        $updatedBook['description']='Description';
        return $updatedBook;
    }

    private function assertNullDataInvalidation($response) {
        $this->assertNullUpdateInvalidation($response);
        $this->assertEquals($response->getData()->errors->book[0], $this->requiredErrorMessage);
        $this->assertEquals($response->getData()->errors->image[0], $this->requiredErrorMessage);
    }

    private function assertNullUpdateInvalidation($response) {
        $response->assertStatus($this->statusCodes[1]);
        $this->assertEquals($response->getData()->errors->name[0], $this->requiredErrorMessage);
        $this->assertEquals($response->getData()->errors->category_id[0], $this->requiredErrorMessage);
    }

    private function assertInvalidData($response) {
        $this->assertUpdateInvalid($response);
        $this->assertEquals($response->getData()->errors->image[0], $this->imageErrorMessage);
    }

    private function assertUpdateInvalid($response) {
        $response->assertStatus($this->statusCodes[1]);
        $this->assertEquals($response->getData()->errors->category_id[0], 'The category id must be at least 1.');
    }

    private function assertNonNumericCategoryInvalidation($response) {
        $this->assertEquals($response->getData()->errors->category_id[0], 'The category id must be a number.');
    }

    private function assertBookExistance($bookData, $bookId) {
        $this->book = Book::find($bookId);
        $this->assertEquals($this->book->name, $bookData['name']);
        $this->assertEquals($this->book->category_id, $bookData['category_id']);
        $this->assertEquals($this->book->description, $bookData['description']);
        TestUtil::assertCategoryExistance($this->book->category, $this->book->category_id);
        Storage::disk(GlobalConsts::DEFUALT_DRIVER)->assertExists($this->book->hash_image);
        Storage::disk(GlobalConsts::DEFUALT_DRIVER)->assertExists($this->book->hash_book);
        return $this->book;
    }

    private function assertBookUpdated($updatedBook, $bookId) {
        $this->book = $this->assertBookExistance($updatedBook, $bookId);
        $this->assertEquals($this->book->hash_image, $updatedBook['hash_image']);
    }

    private function createBook($bookData) {
        $categoryId = TestUtil::createCategory($this->validCategoryData);
        $this->response = $this->postJson(route(GlobalConsts::BOOK_STORE), $this->getBook($bookData, $categoryId));
        return $this->response->viewData('book');
    }

    private function createAndDelete($bookData) {
        $this->book = $this->createBook($bookData);
        $this->response = $this->get(route(GlobalConsts::BOOK_DELETE,$this->book->id));
        return $this->book;
    }

}

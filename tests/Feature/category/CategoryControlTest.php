<?php // namespace Tests\Feature\category;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\model\category\Category;
use Tests\Feature\Utils\TestUtil;
class CategoryControlTest extends TestCase {

    private $categoryData;
    private $response;
    private $validCategoryData = ['category1'];
    private $invalidCategoryData = [null];
    private $statusCodes = [200, 404];
    private $managingCategoriesView="category.managing_categories";
    private $categories="categories";
    use RefreshDatabase;

    public function testGivenInvalidCategoryData_whenCreated_thenWillGiveErrors() {
        //Given
        $this->categoryData = TestUtil::getCategory($this->invalidCategoryData);
        //When
        $this->response = $this->json('post', 'category', $this->categoryData);
        //Then
        $this->assertInvalidCategory($this->response);
    }

    public function testGivenValidCategoryData_whenCreate_thenWillCreated() {
        //Given
        $this->categoryData = TestUtil::getCategory($this->validCategoryData);
        //When
        $this->response = $this->json('post', 'category', $this->categoryData);
        //Then
        TestUtil::assertViewWithData($this->response,$this->managingCategoriesView
                ,$this->categories,$this->validCategoryData[0]);
    }
    
    public function testGivenExistCategory_WhenReachingUpdateView_thenWillBeReachedWithLatestData() {
        //Given
        $categoryId = TestUtil::createCategory($this->validCategoryData);
        //When
        $this->response = $this->get("category/$categoryId/edit");
        //Then
        TestUtil::assertViewExistance('category.update', $this->statusCodes[0],$this->response);
        TestUtil::assertCategoryExistance($this->response->viewData('category'), $categoryId);
    }

    public function testGivenNotExistCategory_WhenReachingUpdateView_thenWillAbort() {
        //Given
        $nonExistCategoryId = 100;
        //When
        $this->response = $this->get("category/$nonExistCategoryId/edit");
        //Then
        $this->response->assertStatus($this->statusCodes[1]);
    }

    public function testGivenInvalidCategoryData_WhenUpdate_thenWillBeUpdated() {
        //Given
        $this->categoryData = TestUtil::getCategory($this->invalidCategoryData);
        //When
        $this->response = $this->json('put', "category/1", $this->categoryData);
        //Then
        $this->assertInvalidCategory($this->response);
    }

    public function testGivenValidCategoryData_WhenUpdate_thenWillBeUpdated() {
        //Given
        $categoryId = TestUtil::createCategory($this->validCategoryData);
        $updatedCategory = TestUtil::getCategory(['updated category']);
        //When
        $this->response = $this->json('put', "category/$categoryId", $updatedCategory);
        //Then
        TestUtil::assertViewWithData($this->response,$this->managingCategoriesView
                ,$this->categories,$updatedCategory['name']);
    }

    public function testGivenExistCategoryId_whenDelete_thenWillDeleted() {
        //Given
        $categoryId = TestUtil::createCategory($this->validCategoryData);
        //When
        $this->response = $this->get("category/$categoryId/delete");
        //Then
        $this->assertNotNull(Category::withTrashed()->find($categoryId));
        $this->assertNull(Category::find($categoryId));
    }

    public function testGivenNonExistCategoryId_whenDelete_thenWillAbort() {
        //Given
        $nonExistCategoryId = 100;
        //When
        $this->response = $this->get("category/$nonExistCategoryId/delete");
        //Then
        $this->response->assertStatus($this->statusCodes[1]);
    }

    public function testGivenCategories_whenAdminRequestManagingAllCategories_thenWillBeReturned() {
        //Given
        TestUtil::createCategory($this->validCategoryData);
        //When
        $this->response = $this->get('category');
        //Then
        TestUtil::assertViewWithData($this->response,$this->managingCategoriesView
                ,$this->categories,$this->validCategoryData[0]);
    }

    public function testGivenCategories_whenAdminRequestDeletedCategories_thenWillReturned() {
        //Given
        $categoryData = ['Category2'];
        $this->createAndDelete($this->validCategoryData);
        TestUtil::createCategory($categoryData);
        //When
        $this->response = $this->get('category/deletedCategories');
        //Then
        TestUtil::assertViewWithData($this->response,$this->managingCategoriesView
                ,$this->categories,$this->validCategoryData[0]);
    }

    public function testGivenDeletedCategory_whenAdminRetrieve_thenWillRetrived() {
        //Given
        $categoryId = $this->createAndDelete($this->validCategoryData);
        //When
        $this->response = $this->get("category/$categoryId/restore");
        //Then
        TestUtil::assertCategoryExistance(TestUtil::getCategory($this->validCategoryData), $categoryId);
    }

    public function testGivenCategoryIdNotExist_whenAdminRetrieve_thenWillAbort() {
        //Given
        $nonExistCategoryId = 100;
        //When
        $this->response = $this->get("category/$nonExistCategoryId/restore");
        //Then
        $this->response->assertStatus($this->statusCodes[1]);
    }

    public function testGivenDeletedCategory_whenAdminPermanentlyDelete_ThenWillDeleted() {
        //Given
        $categoryId = $this->createAndDelete($this->validCategoryData);
        //When
        $this->response = $this->get("category/$categoryId/deletePermanently");
        //Then
        TestUtil::assertViewWithData($this->response,$this->managingCategoriesView
                ,$this->categories,null);
        $this->assertNull(Category::find($categoryId));
    }

    public function testGivenCategoryIdNotExist_whenAdminPermanentlyDelete_ThenWillAbort() {
        //Given
        $nonExistCategoryId = 100;
        //When
        $this->response = $this->get("category/$nonExistCategoryId/deletePermanently");
        //Then
        $this->response->assertStatus($this->statusCodes[1]);
    }

    public function testWhenReachingVerifyDeleteView_thenWillBeReached() {
        //Given
        $cateogryId = 1;
        //When
        $this->response = $this->get("category/$cateogryId/verifyDelete");
        //Then
        TestUtil::assertViewExistance('category.confirm_delete', $this->statusCodes[0], $this->response);
        $this->assertEquals($this->response->viewData('categoryId'), $cateogryId);
    }
    
    //Utils

    private function assertInvalidCategory($response) {
        $response->assertStatus(422);
        $this->assertEquals($this->response->getData()->errors->name[0], "This field is required");
    }

    private function createAndDelete($categoryData) {
        $categoryId = TestUtil::createCategory($categoryData);
        Category::find($categoryId)->delete();
        return $categoryId;
    }
    
    
}

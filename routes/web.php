<?php

use App\Utils\GlobalConsts;

/////////////////////////////////////
//                                 //
//       Category component        //
//                                 //
/////////////////////////////////////
Route::get('category/{categoryId}/delete', 'category\CategoryController@delete');
Route::get('category/{categoryId}/deletePermanently', 'category\CategoryController@deletePermanently');
Route::get('category/{categoryId}/restore', 'category\CategoryController@restore');
Route::get('category/{categoryId}/verifyDelete', 'category\CategoryController@verifyDelete');
Route::get('category/deletedCategories', 'category\CategoryController@getDeletedCategories');
Route::resource('category', 'category\CategoryController');
/////////////////////////////////////
//                                 //
//       Book component            //
//                                 //
/////////////////////////////////////
Route::prefix('books')->group(function() {
    Route::namespace("Book")->group(function() {
        $bookController = "BookController";
        $bookId = "{bookId}";
        Route::post('', "$bookController@store")->name(GlobalConsts::BOOK_STORE);
        Route::get("$bookId/edit", "$bookController@edit")->name(GlobalConsts::BOOK_EDIT);
        Route::put($bookId, "$bookController@update")->name(GlobalConsts::BOOK_UPDATE);
        Route::get('', "$bookController@getAllBooks")->name(GlobalConsts::ALL_BOOKS);
        Route::get('deletedBooks', "$bookController@getDeletedBooks")->name(GlobalConsts::DELETED_BOOKS);
        Route::get("$bookId/delete", "$bookController@delete")->name(GlobalConsts::BOOK_DELETE);
        Route::get("$bookId/restore", "$bookController@restore")->name(GlobalConsts::BOOK_RESTORE);
        Route::get('{categoryId}/categoryBooks', "$bookController@getCategoryBooks")->name(GlobalConsts::CATEGORY_BOOKS);
        Route::get("$bookId/deletePermanently", "$bookController@deletePermanently")
                ->name(GlobalConsts::BOOK_FORCE_DELETE);
        Route::get("$bookId/confirmDelete", "$bookController@verifyDelete")
                ->name(GlobalConsts::BOOK_CONFIRM_DELETE);
    });
});
